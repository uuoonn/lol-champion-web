import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import CardChampionInfo from '~/components/card-champion-info'
Vue.component('CardChampionInfo', CardChampionInfo)
